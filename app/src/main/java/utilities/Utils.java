package utilities;

import android.content.Context;

public class Utils {
	private static Utils utils;
	
	public static Utils getInstance()
	{
		if(utils == null){
			utils = new Utils();
		}
		
		return utils;
	}
	
	public enum ResourceType {
		layout, id, drawable, anim, array;
	}
	
	public int getResource(Context context, String resourceName, ResourceType resourceType)
	{
		return context.getResources().getIdentifier(resourceName, resourceType.toString(), context.getPackageName());
	}
}
