package com.molpay.molpaylib;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Window;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

import utilities.Utils;

public class MOLPayActivity extends FragmentActivity {

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	 
	}


	 
	OnBackPressedListener backPressedListener;
	// OrderInfo
	private String OrderId;
	private float Amount;
	private String Country;
	private String Currency;
	private String Channel;
	private String bill_name;
	private String bill_desc;
	private String bill_email;
	private String bill_mobile;
	private String token;
	// MerchantInfo
	private String MerchantId;
	private String VerifyKey;
	private String AppName, USERNAME, PASSWORD;
	public Bundle bundle, extras;

	static String no_token_frag = "add_card";
	static String token_frag = "list_card";
	private AlertDialog alert, errorAlert;
	boolean debug=false;
	boolean editable=false;
	boolean is_escrow=false;
	private int MOLWallet_Merchant_ID;
	int frag = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);

		try {
			 setContentView(Utils.getInstance().getResource(this, "molpay_main", Utils.ResourceType.layout));
			 frag= Utils.getInstance().getResource(this, "frag", Utils.ResourceType.id);

		} catch (Exception e) {

		}

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				this.getApplicationContext()).build();
		ImageLoader.getInstance().init(config);
		//Intent intent = getIntent();
		 
		extras = getIntent().getExtras();// intent.getExtras();

		if (extras != null) {			
			MerchantId = extras.containsKey("MerchantId") ? extras.getString("MerchantId").trim() : ""; 
			AppName = extras.containsKey("AppName") ? extras.getString("AppName").trim() : "" ;
			VerifyKey = extras.containsKey("VerifyKey") ? extras.getString("VerifyKey").trim() : "" ;
			OrderId = extras.containsKey("OrderId") ? extras.getString("OrderId").trim() : "";
			bill_name = extras.containsKey("BillName") ? extras.getString("BillName").trim() : "";
			bill_desc = extras.containsKey("BillDesc") ? extras.getString("BillDesc").trim() : "";
			bill_mobile = extras.containsKey("BillDesc") ?  extras.getString("BillMobile").trim(): "";
			bill_email = extras.containsKey("BillDesc") ?  extras.getString("BillEmail").trim(): "";
			Channel = extras.containsKey("Channel") ?  extras.getString("Channel").trim() : "";
			Currency = extras.containsKey("Currency") ?  extras.getString("Currency").trim(): "";
			Country = extras.containsKey("Country") ?  extras.getString("Country").trim(): "";
			Amount = extras.containsKey("Amount") ?  extras.getFloat("Amount"): 0;
			USERNAME = extras.containsKey("Username") ?  extras.getString("Username").trim(): "";
			PASSWORD = extras.containsKey("Password") ?  extras.getString("Password").trim(): "";
			debug= extras.containsKey("debug") ?  extras.getBoolean("debug") : false;
			editable= extras.containsKey("editable") ? extras.getBoolean("editable") : false;
			token= extras.containsKey("token") ? extras.getString("token") : "";
			is_escrow= extras.containsKey("is_escrow") ?  extras.getBoolean("is_escrow") : false;
			MOLWallet_Merchant_ID= extras.containsKey("MOLWallet_Merchant_Id") ?  extras.getInt("MOLWallet_Merchant_Id") : -1;
        }
		
		if(debug && isNetworkAvailable())
		{
			new debug().execute();
		}	
		extras.putString("MerchantId", MerchantId);
		extras.putString("AppName", AppName);
		extras.putString("VerifyKey", VerifyKey);
		extras.putString("Username", USERNAME);
		extras.putString("Password", PASSWORD);
		extras.putString("OrderId", OrderId);
		extras.putString("BillName", bill_name);
		extras.putString("BillDesc", bill_desc);
		extras.putString("BillMobile", bill_mobile);
		extras.putString("BillEmail", bill_email);
		extras.putString("Channel", Channel);
		extras.putString("Currency", Currency);
		extras.putString("Country", Country);
		extras.putFloat("Amount",Amount);
		extras.putBoolean("editable",editable);
		extras.putBoolean("is_escrow",is_escrow);
		extras.putInt("MOLWallet_Merchant_ID",	MOLWallet_Merchant_ID );
		//intent.putExtras(extras);
		
		if(!isNetworkAvailable())
		{
			errorDialogExit("Network Issue", "Please make sure that you are connected to the internet");
		}
		else if (Amount <= 1.0f) {
			errorDialogExit("Invalid Amount", "Amount should more than 1.00");
		} 
		else if (bill_name.length() < 1 && !editable) {
			errorDialogExit("Invalid input", "Bill name not found");
		} else if (bill_email.length() < 1 && !editable) {
			errorDialogExit("Invalid input", "Bill email not found");
		} else if (bill_mobile.length() < 1 &&!editable) {
			errorDialogExit("Invalid input", "Bill mobile not found");
		}
		else if (bill_desc.length() < 1 && !editable) {
			errorDialogExit("Invalid input", "Bill description not found");
		}else {
			if (Channel != null) {

				if (Channel.equalsIgnoreCase(no_token_frag)) {

					PaymentCCNoTokenFrag ccNotoken = new PaymentCCNoTokenFrag();
					
					Bundle bundle = new Bundle();
					
					bundle.putBundle("bundle", extras);
					ccNotoken.setArguments(bundle);

					FragmentTransaction transaction = getSupportFragmentManager()
							.beginTransaction();
					
					transaction.add(frag, ccNotoken);
					transaction.addToBackStack(null);

					transaction.commit();

				} else if (Channel.equalsIgnoreCase(token_frag)) {

					new checkToken().execute();

				} else {

					PaymentDetailsFrag paymentDetails = new PaymentDetailsFrag();

					FragmentTransaction transaction = getSupportFragmentManager()
							.beginTransaction();

					transaction.add(frag, paymentDetails);
					transaction.addToBackStack(null);

					transaction.commit();
				}
			} else {
				PaymentDetailsFrag paymentDetails = new PaymentDetailsFrag();

				FragmentTransaction transaction = getSupportFragmentManager()
						.beginTransaction();

				transaction.add(frag, paymentDetails);
				transaction.addToBackStack(null);

				transaction.commit();
			}
		}

		backButton();
		if(isAlwaysFinishActivitiesOptionEnabled())
		{
			showDeveloperOptionsScreen();
		}
		
	}

 
	public void onFinishData(Bundle bndle) {
		bundle = bndle;

		finish();
	}

	@Override
	public void finish() {

		if (bundle != null) {
			Intent data = new Intent();
			data.putExtra("bundle", bundle);
			setResult(RESULT_OK, data);
		}
		super.finish();
	}

	private class checkToken extends AsyncTask<Void, Void, Boolean> {
		String returnResult;
		JSONObject jsonObj;
		ProgressDialog progressDialog;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String url = null;
			returnResult = JSONnString.token(url, extras);

			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			String returnStatus = "";
			progressDialog.dismiss();
			super.onPostExecute(result);
            //System.out.println("the token from sdk is "+returnResult);
			try {
				jsonObj = new JSONObject(returnResult);
				returnStatus = jsonObj.getString("status");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (returnStatus.equals("true")) {

				PaymentCCFrag paymentCCFrag = new PaymentCCFrag();

				FragmentTransaction transaction = getSupportFragmentManager()
						.beginTransaction();

				Bundle bundle = new Bundle();
				bundle.putBundle("bundle", extras);
				bundle.putString("cclist", jsonObj.toString());
				paymentCCFrag.setArguments(bundle);
				transaction.add(frag, paymentCCFrag);
				transaction.addToBackStack(null);
				transaction.commit();
			} else {
				PaymentCCNoTokenFrag paymentCCNoTokenFrag = new PaymentCCNoTokenFrag();

				FragmentTransaction transaction = getSupportFragmentManager()
						.beginTransaction();

				Bundle bundle = new Bundle();
				bundle.putBundle("bundle", extras);
				paymentCCNoTokenFrag.setArguments(bundle);
				transaction.add(frag, paymentCCNoTokenFrag);
				transaction.addToBackStack(null);
				transaction.commit();
			}

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(MOLPayActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Loading...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
		}
	}

	
	private class debug extends AsyncTask<Void, Void, Boolean> {
		String returnResult;
		JSONObject jsonObj;
		ProgressDialog progressDialog;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			String url = null;
		    returnResult = JSONnString.debug(url, extras);
		 
			return null;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			String returnStatus = null;
			progressDialog.dismiss();
			super.onPostExecute(result);
         
			try {
				jsonObj = new JSONObject(returnResult);
				returnStatus = jsonObj.getString("status");
				 
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			 

		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(MOLPayActivity.this);
			progressDialog.setCancelable(false);
			progressDialog.setMessage("Loading...");
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.show();
		}
	}
	private void backButton() {
		//System.out.println("back button pressed on main page");
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(
				"Pressing the back button will close the payment page , Are you sure you want to proceed ? ")
				.setCancelable(false)
				.setPositiveButton("Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								onFinishData(null);
							}
						})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});

		alert = builder.create();

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
        try{
            if ((keyCode == KeyEvent.KEYCODE_BACK)) {
                if(backPressedListener != null){
                    return backPressedListener.backPressed();
                } else {
                    super.onBackPressed();
                }

//			FragmentManager fm = getSupportFragmentManager();
//			if (fm.getBackStackEntryCount() > 1) {
//				// Log.i("MainActivity",
//				// "popping backstack "+fm.getBackStackEntryCount());
//				fm.popBackStack();
//				return false;
//			} else if (fm.getBackStackEntryCount() == 1) {
//				alert.show();
//			} else {
//				// Log.i("MainActivity", "nothing on backstack, calling super");
//				super.onBackPressed();
//			}
            }
        }catch(Exception ex){
            //Log.e("MOLPayActivity", ex.toString());
        }
		return super.onKeyDown(keyCode, event);
	}

	private void errorDialogExit(String title, String error) {
		AlertDialog.Builder builder = new AlertDialog.Builder(
				MOLPayActivity.this);

		builder.setTitle("" + title);
		builder.setMessage("" + error);
		builder.setCancelable(false);
		builder.setNeutralButton("Okay", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int arg1) {
				// TODO Auto-generated method stub

				dialog.dismiss();
				finish();
			}
		});
		errorAlert = builder.create();
		errorAlert.show();
	}
	public  boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	
	private boolean isAlwaysFinishActivitiesOptionEnabled() {
	    int alwaysFinishActivitiesInt = 0;
	    if (Build.VERSION.SDK_INT >= 17) {
	        alwaysFinishActivitiesInt = Settings.System.getInt(getApplicationContext().getContentResolver(), Settings.Global.ALWAYS_FINISH_ACTIVITIES, 0);	        
	    } else {
	        alwaysFinishActivitiesInt = Settings.System.getInt(getApplicationContext().getContentResolver(), Settings.System.ALWAYS_FINISH_ACTIVITIES, 0);	        
	    }

	    if (alwaysFinishActivitiesInt == 1) {
	        return true;
	    } else {
	        return false;
	    }
	}
	private void showDeveloperOptionsScreen(){
		final AlertDialog.Builder builder = new AlertDialog.Builder(MOLPayActivity.this);
		builder.setMessage(
				"Settings -> General -> Developer options -> Apps (Do not keep activities) is enabled. Please click OK to turn it off and click BACK button to resume payment. ?  ")
				.setCancelable(false)
				.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {								 
								alert.dismiss();						
							    Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
							    intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
							    startActivity(intent);
							}
						})
				.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

						dialog.dismiss();

					}
				});
		alert = builder.create();
		alert.show();
		
	}

    public interface OnBackPressedListener {
        public boolean backPressed();
    }
}
