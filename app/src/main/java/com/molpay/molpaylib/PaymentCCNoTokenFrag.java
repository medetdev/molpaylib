package com.molpay.molpaylib;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import utilities.Utils;
import utilities.Utils.ResourceType;

import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class PaymentCCNoTokenFrag extends Fragment implements MOLPayActivity.OnBackPressedListener {

	// OrderInfo
	private String OrderId;
	private float Amount;
	private String Channel;
	private String Currency;
	String cvv, countrycode;

	private TextView txt_amt, txt_orderId;
	private TextView txt_currency;
	private EditText edt_cardNo, edt_cvv;
	private ImageView cardType;
	private Spinner spn_month, spn_year;
	private View details_layout;
	private AutoCompleteTextView edt_bankName, spn_country;
	private Button btn_pay, btn_back;
	JSONArray jArray;
	int type;
	Bundle extras;
	List<String> list, listcode;
	JSONObject jCountry;
	private String[] bank = null;
	private int invalidCard;

	int edtcardNo;
	View v;

	Boolean showCCDetails = false;
	private String defaultCountry = "";
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		extras = this.getArguments().getBundle("bundle");
		
		if (extras != null) {
			OrderId = extras.getString("OrderId");
			Channel = extras.getString("Channel");
			Amount = extras.getFloat("Amount");
			Currency = extras.getString("Currency");
			showCCDetails = extras.containsKey("ShowCCDetails") ? extras.getBoolean("ShowCCDetails") : false;
			defaultCountry = extras.containsKey("DefaultCountry") ? extras.getString("DefaultCountry") : "";
		}

		try {

			v = inflater.inflate(Utils.getInstance().getResource(getActivity(), "payment_cc_notoken", ResourceType.layout), container, false);

			details_layout = v.findViewById(Utils.getInstance().getResource(getActivity(), "details", ResourceType.id));
			txt_orderId = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_orderId", ResourceType.id));
			txt_amt = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_amt", ResourceType.id));
			txt_currency = (TextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "txt_currency", ResourceType.id));
			edt_cardNo = (EditText) v.findViewById(Utils.getInstance().getResource(getActivity(), "edt_cardNo", ResourceType.id));
			edt_cvv = (EditText) v.findViewById(Utils.getInstance().getResource(getActivity(), "edt_cvv", ResourceType.id));
			cardType = (ImageView) v.findViewById(Utils.getInstance().getResource(getActivity(), "img_cardType", ResourceType.id));
			spn_country = (AutoCompleteTextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "paymentOpt", ResourceType.id));
			spn_month = (Spinner) v.findViewById(Utils.getInstance().getResource(getActivity(), "spinner_month", ResourceType.id));
			spn_year = (Spinner) v.findViewById(Utils.getInstance().getResource(getActivity(), "spinner_year", ResourceType.id));
			edt_bankName = (AutoCompleteTextView) v.findViewById(Utils.getInstance().getResource(getActivity(), "edt_bankName", ResourceType.id));
			btn_pay = (Button) v.findViewById(Utils.getInstance().getResource(getActivity(), "btn_pay", ResourceType.id));
			btn_back = (Button) v.findViewById(Utils.getInstance().getResource(getActivity(), "btn_back", ResourceType.id));
		} catch (Exception e) {

		}

		String amt = null;
		
		if(Currency.equals("IDR"))
		{
			amt=JSONnString.toRupiahFormat(String.valueOf(Amount),false);
			txt_amt.setTextSize(35);
		}
		else
		{

		amt = JSONnString.printAmount(Amount);
		}
		
		
		txt_currency.setText(Currency);
		txt_amt.setText("" + amt);
		txt_orderId.setText(OrderId);

		edt_cardNo.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub
				if (s.length() == 1) {

					type = Integer.parseInt(s.toString());

					new setCardImg().execute(type);

				}

				if (s.length() == 16) {

					byte cardT = 0;

					if (type == 4) {
						cardT = Validator.VISA; // Visa
					} else {
						cardT = Validator.MASTERCARD; // Mastercard
					}

					if (Validator.validate(edt_cardNo.getText().toString(),
							cardT)) {
						invalidCard = 1;
						// PayNow pnow2 = new PayNow();
						//
						// pnow2.execute();
					} else {

						invalidCard = 0; // card no invalid
						Animation shake = AnimationUtils.loadAnimation(
								getActivity(), Utils.getInstance().getResource(getActivity(), "shake", ResourceType.anim));
						// Animation shake = AnimationUtils.loadAnimation(
						// getActivity(), R.anim.shake);
						shake.setAnimationListener(new AnimationListener() {

							@Override
							public void onAnimationEnd(Animation arg0) {
								// TODO Auto-generated method stub
								String card;
								if (type == 4) {
									card = " Visa";
								} else if (type == 5) {
									card = " Mastercard";
								} else {
									card = "";
								}
								AlertDialog.Builder builder = new AlertDialog.Builder(
										getActivity());
								builder.setMessage(
										"Your" + card
												+ " card number is not valid")
										.setCancelable(false)
										.setPositiveButton(
												"OK",
												new DialogInterface.OnClickListener() {
													public void onClick(
															DialogInterface dialog,
															int id) {
													}
												});

								AlertDialog alert = builder.create();
								alert.show();
							}

							@Override
							public void onAnimationRepeat(Animation arg0) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationStart(Animation arg0) {
								// TODO Auto-generated method stub

							}
						});
						edt_cardNo.startAnimation(shake);
					}
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub

			}

		});

		list = new ArrayList<String>();
		// listcode = new ArrayList<String>();

		String[] locales = Locale.getISOCountries();

		jCountry = new JSONObject();
		
		if(!showCCDetails){
			details_layout.setVisibility(View.GONE);
		} else{
			details_layout.setVisibility(View.VISIBLE);
			
			String countryName = "MALAYSIA"; 
			
			
			list.add(0, "MALAYSIA");
			
			for(int i = 0; i < locales.length; i ++)
			{
				String countryCode = locales[i];
				Locale obj = new Locale("", countryCode);

				String country = "" + obj.getDisplayCountry().toUpperCase(Locale.getDefault());
				String ccode = "" + obj.getCountry().toUpperCase(Locale.getDefault());

				try {
					jCountry.put(country, ccode);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (!country.equalsIgnoreCase("MALAYSIA")) {
					list.add("" + country);
				}
				
				if (country.equalsIgnoreCase(defaultCountry)) {
					countryName = country;
				}
			}

			ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
					android.R.layout.simple_list_item_1, list);
			spn_country.setAdapter(adapter);
			spn_country.setText(countryName);
			
			if (countryName.equalsIgnoreCase("Malaysia")) {
				new loadMsianBank().execute();
			}
			
			spn_country.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> view, View view2,
						int position, long arg3) {
					// TODO Auto-generated method stub
					String select = view.getItemAtPosition(position).toString();
					// list.get(position).get("ccode");
					// Log.i("cc", "" + list.get(position).get("ccode"));

					if (select.equalsIgnoreCase("Malaysia")) {
						new loadMsianBank().execute();
					} else {
						if (edt_bankName.getText().length() > 0) {
							edt_bankName.setText("");
						}
						edt_bankName.setAdapter((ArrayAdapter<String>) null);
					}
				}
			});
		}



		ArrayAdapter<CharSequence> month_adapter = ArrayAdapter
				.createFromResource(getActivity(), Utils.getInstance().getResource(getActivity(), "date_month", ResourceType.array), 
						Utils.getInstance().getResource(getActivity(), "list_item", ResourceType.layout));
		month_adapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spn_month.setAdapter(month_adapter);


		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		
		String[] yearArray = new String[12];
		
		yearArray[0] = "Year";
		yearArray[1] = String.valueOf(year);
		year++;
		
		for(int i = 2; i < yearArray.length; i++)
		{
			yearArray[i] = String.valueOf(year);
			year++;
		}
		ArrayAdapter<String> year_adapter = new ArrayAdapter<String>(getActivity(), Utils.getInstance().getResource(getActivity(), "list_item", ResourceType.layout), yearArray);
		
		year_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spn_year.setAdapter(year_adapter);

		btn_pay.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub

				String cardNo = edt_cardNo.getText().toString();
				cvv = edt_cvv.getText().toString();
				String mm = spn_month.getSelectedItem().toString();
				String yyyy = spn_year.getSelectedItem().toString();
				String country = spn_country.getText().toString();
				String bank = edt_bankName.getText().toString().trim();
				String ccode = null;
				try {
					ccode = jCountry.getString(country);
					// Log.i("cc", "" + ccode);
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				// {"result":[{"expdate":"1804","bank_country":"MY","bin":"428332","issuer_bank":"MALAYAN BANKING BERHAD","token":"3866676798676593","debit_credit":"DEBIT","bin4":"6593"}],"status":true}

				String errorText = "";
				if (cardNo.length() == 0) {
					errorText = "Please enter your credit card number";
				} else {
					if (invalidCard == 0 || cardNo.length() < 16) {
						errorText = "Your credit card number is invalid";
					} else {
						if (cvv.length() == 0 || cvv.length() < 3) {
							errorText = "Please enter your cvv number / invalid";
						} else {
							if (mm.equalsIgnoreCase("Month")) {
								errorText = "Please select your credit card expiration month";
							} else {
								if (yyyy.equalsIgnoreCase("Year")) {
									errorText = "Please select your credit card expiration year";
								} else {
									//if (country.length() == 0) {
									//	errorText = "Please enter your credit card bank country";
									//} else {
										//if (bank.length() == 0) {
										//	errorText = "Please enter your credit card bank";
										//} else {
											jArray = new JSONArray();

											JSONObject jobt = new JSONObject();

											try {
												jobt.put("expdate", "" + yyyy
														+ mm);
												jobt.put("bank_country", ""
														+ ccode);
												jobt.put("issuer_bank", ""
														+ bank);
												jobt.put("cardnumber", ""
														+ cardNo);

												jArray.put(jobt);

												// jobt.put("result", jArray);
											} catch (JSONException e) {
												// TODO Auto-generated catch
												// block
												e.printStackTrace();
											}
											new PayNow().execute();
										//}
									//}
								}
							}
						}
					}
				}

				if ((errorText.length()) > 0) {
					errorDialog(errorText);
				}

			}
		});

		btn_back.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (Channel.equalsIgnoreCase("list_card")
						|| Channel.equalsIgnoreCase("add_card")) {
					((MOLPayActivity) getActivity()).onFinishData(null);
				} else {
					getFragmentManager().popBackStack();
				}
			}
		});

		return v;
	}

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MOLPayActivity)getActivity()).backPressedListener = this;
    }

    @Override
    public boolean backPressed() {
        if (Channel.equalsIgnoreCase("list_card")
                || Channel.equalsIgnoreCase("add_card")) {
            ((MOLPayActivity) getActivity()).onFinishData(null);
            return true;
        } else {
            getFragmentManager().popBackStack();
            return false;
        }
    }

    protected class setCardImg extends AsyncTask<Integer, Void, Boolean> {

		Integer typea = 0;

		@Override
		protected Boolean doInBackground(Integer... intType) {
			typea = intType[0];
			new Thread() {
				public void run() {
					try {
						getActivity().runOnUiThread(new Runnable() {

							@Override
							public void run() {
								if (typea == 4) {
									cardType.setImageResource(Utils.getInstance().getResource(getActivity(), "visa", ResourceType.drawable));
								} else if (typea == 5) {
									cardType.setImageResource(Utils.getInstance().getResource(getActivity(), "mastercard", ResourceType.drawable));
								} else {
									cardType.setImageResource(Utils.getInstance().getResource(getActivity(), "ccard", ResourceType.drawable));
								}
							}
						});
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}.start();
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

		}
	}

	private class loadMsianBank extends AsyncTask<Void, Void, Boolean> {

		// JsonObject jArray = null;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			try
			{
			msiaBankPost();
			}
			catch(Exception e)
			{
				Log.i("Exception",	""+e);
			}

			// JsonParser parser = new JsonParser();
			// if (!result.equals("")) {
			// jArray = (JsonObject) parser.parse(result);
			// }
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			try
			{
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(
					getActivity(), android.R.layout.simple_list_item_1, bank);
			edt_bankName.setAdapter(adapter);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	private String msiaBankPost() {
		String result = "";

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000); // Timeout
		// Limit
		HttpResponse response;
		try {

			URI url = new URI(
					"https://www.onlinepayment.com.my/MOLPay/G/resources/misc/get_mybank.php");

			HttpPost post = new HttpPost(url);
			post.setHeader("Content-type", "application/json");

			// StringEntity se = new StringEntity(json.toString());
			// se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
			// "application/json"));
			// post.setEntity(se);
			response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				result = JSONnString.convertStreamToString(in);
			}

			bank = Html.fromHtml(result).toString().split("\\n");

			post.abort();
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private class PayNow extends AsyncTask<Void, Void, Boolean> {

		// JsonObject jArray = null;

		String HTMLResult;

		@Override
		protected Boolean doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			
			HTMLResult = JSONnString
					.paymentHttp(extras, jArray.toString(), cvv);
			return true;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);

			if (result) {
				PaymentWebViewFrag webviewFrag = new PaymentWebViewFrag();

				FragmentTransaction transaction = getFragmentManager()
						.beginTransaction();

				Bundle bundle = new Bundle();
				extras.putString("Channel", "credit");
				bundle.putBundle("bundle", extras);
				bundle.putString("web", HTMLResult);
				webviewFrag.setArguments(bundle);
				transaction.replace(Utils.getInstance().getResource(getActivity(), "frag", ResourceType.id), webviewFrag);

				transaction.commit();
			}
		}
	}

	private void errorDialog(String ErrorText) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Error");
		builder.setMessage("" + ErrorText)
				.setCancelable(true)
				.setPositiveButton("Okay",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
							}
						});

		AlertDialog alert2 = builder.create();
		alert2.show();

	}


}
