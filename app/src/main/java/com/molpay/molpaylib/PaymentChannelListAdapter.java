package com.molpay.molpaylib;

import android.app.Activity;
import android.content.ClipData.Item;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.molpay.molpaylib.PaymentDetailsFrag.paymentChannelList;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import utilities.Utils;
import utilities.Utils.ResourceType;

public class PaymentChannelListAdapter extends BaseAdapter {
	// private Context context;
	Context context;
	public LayoutInflater inflater;
	int layoutResourceId;
	ArrayList<Item> data = new ArrayList<Item>();
	ArrayList<Object> itemList;
	DisplayImageOptions options;
	protected ImageLoader imageLoader;
	
	public PaymentChannelListAdapter(Activity context,
			ArrayList<Object> itemList) {
		super();

		this.context = context;
		this.itemList = itemList;
		this.inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		//imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		imageLoader = ImageLoader.getInstance();
		
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return itemList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return itemList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	class ViewHolder {
		ImageView imgChannel;
		
		// private TextView userName;
		// private TextView text;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		final ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();

			try {
				convertView = inflater.inflate(Utils.getInstance().getResource(context, "payment_list", ResourceType.layout), null);
				holder.imgChannel = (ImageView) convertView
						.findViewById(Utils.getInstance().getResource(context, "imgChannel", ResourceType.id));
			} catch (Exception e) {

			}

			options = new DisplayImageOptions.Builder()
			.cacheInMemory(true)
			.cacheOnDisc(true)
			.considerExifParams(true)
			.bitmapConfig(Bitmap.Config.RGB_565)
			.build();
			convertView.setTag(holder);

		} else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		paymentChannelList payment = (paymentChannelList) itemList
				.get(position);

		imageLoader.displayImage(payment.getImg(), holder.imgChannel, options);

		return convertView;
	}
}
