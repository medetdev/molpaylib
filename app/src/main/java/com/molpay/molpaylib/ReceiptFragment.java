package com.molpay.molpaylib;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.molpay.molpaylib.settings.MerchantInfo;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import utilities.Utils;
import utilities.Utils.ResourceType;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReceiptFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReceiptFragment extends Fragment implements MOLPayActivity.OnBackPressedListener {
    private static final String BARCODE_LINK = "https://onlinepayment.com.my/MOLPay/barcode_frankie.php?auth=molpayKEY001&barcode=";
    String transactionId, verificationCode, payableAmount, currency, expiredAt;
    private DisplayImageOptions options;
    private AlertDialog alert;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment ReceiptFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReceiptFragment newInstance(Bundle args) {
        ReceiptFragment fragment = new ReceiptFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public ReceiptFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            transactionId = getArguments().getString(MerchantInfo.TXN_ID);
            verificationCode = getArguments().getString(MerchantInfo.P_CODE);
            payableAmount = getArguments().getString(MerchantInfo.PAYABLE_AMOUNT);
            currency = getArguments().getString(MerchantInfo.CURRENCY);
            expiredAt =  getArguments().getString(MerchantInfo.EXPIRED_AT);
        }
        options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565).build();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(Utils.getInstance().getResource(getActivity(), "fragment_receipt", ResourceType.layout), container, false);
        ImageView transactionBarcodePreview = (ImageView)view.findViewById(Utils.getInstance().getResource(getActivity(), "transaction_barcode_preview", ResourceType.id));
        ImageView verificationBarcodePreview = (ImageView)view.findViewById(Utils.getInstance().getResource(getActivity(), "verification_barcode_preview", ResourceType.id));
        TextView payableAmountTextView = (TextView)view.findViewById(Utils.getInstance().getResource(getActivity(), "payable_amount", ResourceType.id));
        TextView transactionText = (TextView)view.findViewById(Utils.getInstance().getResource(getActivity(), "transaction_barcode_text", ResourceType.id));
        TextView verificationText = (TextView)view.findViewById(Utils.getInstance().getResource(getActivity(), "verification_barcode_text", ResourceType.id));
        TextView expiredAtText = (TextView)view.findViewById(Utils.getInstance().getResource(getActivity(), "paydate_textview", ResourceType.id));
        try {
            transactionText.setText(transactionId);
            verificationText.setText(verificationCode);
            ImageLoader.getInstance().displayImage(BARCODE_LINK + transactionId, transactionBarcodePreview, options);
            ImageLoader.getInstance().displayImage(BARCODE_LINK + verificationCode, verificationBarcodePreview, options);
            payableAmountTextView.setText(String.format("%s%s*", currency, payableAmount));
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm", Locale.UK);
            Date date = dateFormat.parse(expiredAt);
            expiredAtText.setText(String.format("** Please pay before %s", dateFormat.format(date)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MOLPayActivity)getActivity()).backPressedListener = this;
    }

    @Override
    public boolean backPressed() {
        if (getActivity() != null && getArguments() != null)
        ((MOLPayActivity)getActivity()).onFinishData(getArguments());
        return true;
    }
}