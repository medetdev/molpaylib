package com.molpay.molpaylib.settings;

public class MerchantInfo {

	// Order Info
	public static String ORDER_ID = "order_id";
	public static String AMOUNT = "amount";
	public static String CURRENCY = "currency";
	public static String COUNTRY = "country";
	public static String BILL_NAME = "billName";
	public static String BILL_DESC = "billDesc";
	public static String BILL_EMAIL = "billEmail";
	public static String BILL_MOBILE = "billMobile";
	public static String CHANNEL = "channel";

	// Payment Info
    public static String PAYABLE_AMOUNT = "payable_amt";
    public static String EXPIRED_AT = "expired_at";
    public static String P_CODE = "pcode";
	public static String TXN_ID = "txn_ID";
	public static String PAY_AMOUNT = "amount";
	public static String STATUS_CODE = "status_code";
	public static String ERR_DESC = "err_desc";
	public static String APP_CODE = "app_code";
	public static String PAYDATE = "paydate";
}