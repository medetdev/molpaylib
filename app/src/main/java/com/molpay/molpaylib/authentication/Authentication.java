package com.molpay.molpaylib.authentication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.security.MessageDigest;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;

import android.util.Log;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

//import com.molpay.molpaylib.settings.MerchantInfo;

public class Authentication {

	private String verification_phrase;
	private String merchant_id, appName, verifyKey;// ,password;
	private String request = "https://www.onlinepayment.com.my/MOLPay/API/mobile/index.php";

	public Authentication(String merchant_id, String appName, String verifyKey) {
		this.merchant_id = merchant_id;
		this.appName = appName;
		this.verifyKey = verifyKey;

	}

	public boolean requestAuth() {
		String combinationMsg = merchant_id + appName + verifyKey;
		verification_phrase = string2MD5(combinationMsg);
		//System.out.println("verification_phrase" + verification_phrase);
		return postAuth();
	}

	private String string2MD5(String md5) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes("UTF-8"));
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100)
						.substring(1, 3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		} catch (UnsupportedEncodingException ex) {
		}
		return null;
	}

	private boolean postAuth() {

		String result = "";

		HttpClient client = new DefaultHttpClient();
		HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
		// // Timeout
		// Limit
		JsonObject jArray = null;
		HttpResponse response;
		JsonObject json = new JsonObject();
		try {
			URI url = new URI(request);

			HttpPost post = new HttpPost(url);
			json.addProperty("merchant_id", merchant_id);
			json.addProperty("verification_phrase", verification_phrase);
			json.addProperty("app_name", appName);
			json.addProperty("msgType", "A1");

			post.setHeader("Content-type", "application/json");

			StringEntity se = new StringEntity(json.toString());
			se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
					"application/json"));
			post.setEntity(se);
			response = client.execute(post);

			if (response != null) {
				InputStream in = response.getEntity().getContent();
				result = convertStreamToString(in);
			}

			JsonParser parser = new JsonParser();
			
		 
			if (!result.equals("")) {
				jArray = (JsonObject) parser.parse(result);
			}

			post.abort();

		} catch (Exception e) {
			e.printStackTrace();
		}

		if (jArray != null) {
			String ServerResponse = parseJsonObject(jArray);
			if (ServerResponse.equals("failed")) {
				return false;
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	public static String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	private String parseJsonObject(JsonObject json) {

		String response;
		response = json.get("verification_response").getAsString();
		return response;
	}
}